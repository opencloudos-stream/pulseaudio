%undefine _strict_symbol_defs_build
%global multilib_archs x86_64 ppc64le
%global bash_completionsdir %(pkg-config --variable=completionsdir bash-completion 2>/dev/null || echo '/etc/bash_completion.d')

Summary:        Improved Linux Sound Server
Name:           pulseaudio
Version:        16.1
Release:        7%{?dist}
License:        LGPLv2+
URL:            http://www.freedesktop.org/wiki/Software/PulseAudio
Source0:        http://freedesktop.org/software/pulseaudio/releases/pulseaudio-%{version}.tar.xz

# https://gitlab.freedesktop.org/pulseaudio/pulseaudio/-/commit/84f5b742e39ba3e375bac9144e0243b7331f4019
Patch0000:      fix-volume-test-fail-with-impossible-alignments.patch

Patch3000:      pulseaudio-autostart.patch
Patch3001:      pulseaudio-11.1-autospawn_disable.patch


BuildRequires:  meson >= 0.50.0 gcc g++ pkgconfig(bash-completion) libtool-ltdl-devel m4
BuildRequires:  intltool pkgconfig pkgconfig(bluez) >= 5.0 xorg-x11-proto-devel
BuildRequires:  doxygen xmltoman libsndfile-devel glib2-devel
BuildRequires:  alsa-lib-devel avahi-devel sbc-devel libXt-devel libXtst-devel
BuildRequires:  libatomic_ops-static, libatomic_ops-devel
BuildRequires:  libXi-devel libSM-devel libX11-devel libICE-devel xcb-util-devel
BuildRequires:  openssl-devel orc-devel libtdb-devel
BuildRequires:  pkgconfig(speexdsp) >= 1.2 libasyncns-devel
BuildRequires:  systemd-devel >= 184 systemd dbus-devel libcap-devel
%{?systemd_requires}
BuildRequires:  pkgconfig(fftw3f) pkgconfig(webrtc-audio-processing) >= 0.2
BuildRequires:  pkgconfig(check) pkgconfig(gstreamer-1.0) >= 1.16.0
BuildRequires:  pkgconfig(gstreamer-app-1.0) >= 1.16.0 pkgconfig(gstreamer-rtp-1.0) >= 1.16.0

Requires(pre):  shadow-utils
Requires:       %{name}-libs = %{version}-%{release}
Requires:       rtkit

Provides:       pulseaudio-daemon

%description
PulseAudio is a sound server for Linux and other Unix like operating
systems. It is intended to be an improved drop-in replacement for the
Enlightened Sound Daemon (ESOUND).

%package qpaeq
Summary:	Pulseaudio equalizer interface
Requires: 	%{name} = %{version}-%{release}
Requires:	python3-qt5-base
Requires:	python3-dbus

%description qpaeq
qpaeq is a equalizer interface for pulseaudio's equalizer sinks.

%package module-x11
Summary:        X11 support for the PulseAudio sound server
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-utils

%description module-x11
X11 bell and security modules for the PulseAudio sound server.

%package module-zeroconf
Summary:        Zeroconf support for the PulseAudio sound server
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-utils

%description module-zeroconf
Zeroconf publishing module for the PulseAudio sound server.

%package module-bluetooth
Summary:        Bluetooth support for the PulseAudio sound server
Requires:       %{name} = %{version}-%{release}
Requires:       bluez >= 5.0

%description module-bluetooth
Contains Bluetooth audio (A2DP/HSP/HFP) support for the PulseAudio sound server.

%package module-gsettings
Summary:        Gsettings support for the PulseAudio sound server
Requires:       %{name} = %{version}-%{release}
%description module-gsettings
GSettings configuration backend for the PulseAudio sound server.

%package libs
Summary:        Libraries for PulseAudio clients
License:        LGPLv2+

%description libs
This package contains the runtime libraries for any application that wishes
to interface with a PulseAudio sound server.

%package libs-glib2
Summary:        GLIB 2.x bindings for PulseAudio clients
License:        LGPLv2+
Requires:       %{name}-libs = %{version}-%{release}

%description libs-glib2
This package contains bindings to integrate the PulseAudio client library with
a GLIB 2.x based application.

%package libs-devel
Summary:        Headers and libraries for PulseAudio client development
License:        LGPLv2+
Requires:       %{name}-libs = %{version}-%{release}
Requires:       %{name}-libs-glib2 = %{version}-%{release}

%description libs-devel
Headers and libraries for developing applications that can communicate with
a PulseAudio sound server.

%package utils
Summary:        PulseAudio sound server utilities
License:        LGPLv2+
Requires:       %{name}-libs = %{version}-%{release}

%description utils
This package contains command line utilities for the PulseAudio sound server.

%prep
%autosetup -p1


sed -i.no_consolekit -e \
  's/^load-module module-console-kit/#load-module module-console-kit/' \
  src/daemon/default.pa.in


%build
%meson \
  -D system_user=pulse \
  -D system_group=pulse \
  -D access_group=pulse-access \
  -D oss-output=disabled \
  -D jack=disabled \
  -D lirc=disabled \
  -D tcpwrap=disabled \
  -D bluez5=enabled \
  -D gstreamer=enabled \
  -D bluez5-gstreamer=enabled \
  -D gsettings=enabled \
  -D elogind=disabled \
  -D valgrind=disabled \
  -D gtk=disabled \
  -D soxr=disabled \
  -D webrtc-aec=enabled \
  -D systemd=enabled \
  -D tests=true

%meson_build

%meson_build doxygen

%install
%meson_install

mkdir -p %{buildroot}%{_prefix}/lib/udev/rules.d
mv -fv %{buildroot}/lib/udev/rules.d/90-pulseaudio.rules %{buildroot}%{_prefix}/lib/udev/rules.d

rm -fv %{buildroot}%{_libdir}/lib*.la
rm -fv %{buildroot}%{_libdir}/pulseaudio/lib*.la
rm -fv %{buildroot}%{_libdir}/pulseaudio/modules/*.la
rm -fv %{buildroot}%{_libdir}/pulseaudio/modules/module-detect.so

%find_lang %{name}

%check
%meson_test || exit 1


%pre
getent group pulse-access >/dev/null || groupadd -r pulse-access
getent group pulse-rt >/dev/null || groupadd -r pulse-rt
getent group pulse >/dev/null || groupadd -f -g 171 -r pulse
if ! getent passwd pulse >/dev/null ; then
    if ! getent passwd 171 >/dev/null ; then
      useradd -r -u 171 -g pulse -d %{_localstatedir}/run/pulse -s /sbin/nologin -c "PulseAudio System Daemon" pulse
    else
      useradd -r -g pulse -d %{_localstatedir}/run/pulse -s /sbin/nologin -c "PulseAudio System Daemon" pulse
    fi
fi
exit 0

%posttrans
(grep '^load-module module-cork-music-on-phone$' %{_sysconfdir}/pulse/default.pa > /dev/null && \
 sed -i.rpmsave -e 's|^load-module module-cork-music-on-phone$|load-module module-role-cork|' \
 %{_sysconfdir}/pulse/default.pa
) ||:

%post
%systemd_user_post pulseaudio.socket

%preun
%systemd_user_preun pulseaudio.socket


%files
%license LICENSE GPL LGPL
%doc README
%config(noreplace) %{_sysconfdir}/pulse/daemon.conf
%config(noreplace) %{_sysconfdir}/pulse/default.pa
%config(noreplace) %{_sysconfdir}/pulse/system.pa
%{_sysconfdir}/dbus-1/system.d/pulseaudio-system.conf
%{bash_completionsdir}/pulseaudio
%{_userunitdir}/pulseaudio.service
%{_userunitdir}/pulseaudio.socket
%{_bindir}/pa-info
%{_bindir}/pulseaudio
%{_libdir}/pulseaudio/libpulsecore-%{version}.so
%dir %{_libdir}/pulseaudio/
%dir %{_libdir}/pulseaudio/modules/
%{_libdir}/pulseaudio/modules/libalsa-util.so
%{_libdir}/pulseaudio/modules/libcli.so
%{_libdir}/pulseaudio/modules/libprotocol-cli.so
%{_libdir}/pulseaudio/modules/libprotocol-http.so
%{_libdir}/pulseaudio/modules/libprotocol-native.so
%{_libdir}/pulseaudio/modules/libprotocol-simple.so
%{_libdir}/pulseaudio/modules/librtp.so
%{_libdir}/pulseaudio/modules/libwebrtc-util.so
%{_libdir}/pulseaudio/modules/module-allow-passthrough.so
%{_libdir}/pulseaudio/modules/module-alsa-sink.so
%{_libdir}/pulseaudio/modules/module-alsa-source.so
%{_libdir}/pulseaudio/modules/module-alsa-card.so
%{_libdir}/pulseaudio/modules/module-cli-protocol-tcp.so
%{_libdir}/pulseaudio/modules/module-cli-protocol-unix.so
%{_libdir}/pulseaudio/modules/module-cli.so
%{_libdir}/pulseaudio/modules/module-combine.so
%{_libdir}/pulseaudio/modules/module-combine-sink.so
%{_libdir}/pulseaudio/modules/module-dbus-protocol.so
%{_libdir}/pulseaudio/modules/module-filter-apply.so
%{_libdir}/pulseaudio/modules/module-filter-heuristics.so
%{_libdir}/pulseaudio/modules/module-device-manager.so
%{_libdir}/pulseaudio/modules/module-loopback.so
%{_libdir}/pulseaudio/modules/module-udev-detect.so
%{_libdir}/pulseaudio/modules/module-hal-detect.so
%{_libdir}/pulseaudio/modules/module-http-protocol-tcp.so
%{_libdir}/pulseaudio/modules/module-http-protocol-unix.so
%{_libdir}/pulseaudio/modules/module-match.so
%{_libdir}/pulseaudio/modules/module-mmkbd-evdev.so
%{_libdir}/pulseaudio/modules/module-native-protocol-fd.so
%{_libdir}/pulseaudio/modules/module-native-protocol-tcp.so
%{_libdir}/pulseaudio/modules/module-native-protocol-unix.so
%{_libdir}/pulseaudio/modules/module-null-sink.so
%{_libdir}/pulseaudio/modules/module-null-source.so
%{_libdir}/pulseaudio/modules/module-pipe-sink.so
%{_libdir}/pulseaudio/modules/module-pipe-source.so
%{_libdir}/pulseaudio/modules/module-remap-source.so
%{_libdir}/pulseaudio/modules/module-rescue-streams.so
%{_libdir}/pulseaudio/modules/module-role-ducking.so
%{_libdir}/pulseaudio/modules/module-rtp-recv.so
%{_libdir}/pulseaudio/modules/module-rtp-send.so
%{_libdir}/pulseaudio/modules/module-simple-protocol-tcp.so
%{_libdir}/pulseaudio/modules/module-simple-protocol-unix.so
%{_libdir}/pulseaudio/modules/module-sine.so
%{_libdir}/pulseaudio/modules/module-switch-on-port-available.so
%{_libdir}/pulseaudio/modules/module-systemd-login.so
%{_libdir}/pulseaudio/modules/module-tunnel-sink-new.so
%{_libdir}/pulseaudio/modules/module-tunnel-sink.so
%{_libdir}/pulseaudio/modules/module-tunnel-source-new.so
%{_libdir}/pulseaudio/modules/module-tunnel-source.so
%{_libdir}/pulseaudio/modules/module-volume-restore.so
%{_libdir}/pulseaudio/modules/module-suspend-on-idle.so
%{_libdir}/pulseaudio/modules/module-default-device-restore.so
%{_libdir}/pulseaudio/modules/module-device-restore.so
%{_libdir}/pulseaudio/modules/module-stream-restore.so
%{_libdir}/pulseaudio/modules/module-card-restore.so
%{_libdir}/pulseaudio/modules/module-ladspa-sink.so
%{_libdir}/pulseaudio/modules/module-remap-sink.so
%{_libdir}/pulseaudio/modules/module-always-sink.so
%{_libdir}/pulseaudio/modules/module-always-source.so
%{_libdir}/pulseaudio/modules/module-console-kit.so
%{_libdir}/pulseaudio/modules/module-position-event-sounds.so
%{_libdir}/pulseaudio/modules/module-augment-properties.so
%{_libdir}/pulseaudio/modules/module-role-cork.so
%{_libdir}/pulseaudio/modules/module-sine-source.so
%{_libdir}/pulseaudio/modules/module-intended-roles.so
%{_libdir}/pulseaudio/modules/module-rygel-media-server.so
%{_libdir}/pulseaudio/modules/module-echo-cancel.so
%{_libdir}/pulseaudio/modules/module-switch-on-connect.so
%{_libdir}/pulseaudio/modules/module-virtual-sink.so
%{_libdir}/pulseaudio/modules/module-virtual-source.so
%{_libdir}/pulseaudio/modules/module-virtual-surround-sink.so
%dir %{_datadir}/pulseaudio/
%dir %{_datadir}/pulseaudio/alsa-mixer/
%{_datadir}/pulseaudio/alsa-mixer/paths/
%{_datadir}/pulseaudio/alsa-mixer/profile-sets/
%{_prefix}/lib/udev/rules.d/90-pulseaudio.rules
%dir %{_libexecdir}/pulse
%dir %{_datadir}/zsh/
%dir %{_datadir}/zsh/site-functions/
%{_datadir}/zsh/site-functions/_pulseaudio
%{_mandir}/man1/pulseaudio.1*
%{_mandir}/man5/default.pa.5*
%{_mandir}/man5/pulse-cli-syntax.5*
%{_mandir}/man5/pulse-client.conf.5*
%{_mandir}/man5/pulse-daemon.conf.5*

%files qpaeq
%{_bindir}/qpaeq
%{_libdir}/pulseaudio/modules/module-equalizer-sink.so

%files module-x11
%config(noreplace) %{_sysconfdir}/xdg/autostart/pulseaudio.desktop
%config(noreplace) %{_sysconfdir}/xdg/Xwayland-session.d/00-pulseaudio-x11
%{_userunitdir}/pulseaudio-x11.service
%{_bindir}/start-pulseaudio-x11
%{_libdir}/pulseaudio/modules/module-x11-bell.so
%{_libdir}/pulseaudio/modules/module-x11-publish.so
%{_libdir}/pulseaudio/modules/module-x11-xsmp.so
%{_libdir}/pulseaudio/modules/module-x11-cork-request.so
%{_mandir}/man1/start-pulseaudio-x11.1.gz

%files module-zeroconf
%{_libdir}/pulseaudio/modules/libavahi-wrap.so
%{_libdir}/pulseaudio/modules/module-zeroconf-publish.so
%{_libdir}/pulseaudio/modules/module-zeroconf-discover.so
%{_libdir}/pulseaudio/modules/libraop.so
%{_libdir}/pulseaudio/modules/module-raop-discover.so
%{_libdir}/pulseaudio/modules/module-raop-sink.so

%files module-bluetooth
%{_libdir}/pulseaudio/modules/libbluez*-util.so
%{_libdir}/pulseaudio/modules/module-bluez*-device.so
%{_libdir}/pulseaudio/modules/module-bluez*-discover.so
%{_libdir}/pulseaudio/modules/module-bluetooth-discover.so
%{_libdir}/pulseaudio/modules/module-bluetooth-policy.so

%files module-gsettings
%{_libdir}/pulseaudio/modules/module-gsettings.so
%{_libexecdir}/pulse/gsettings-helper
%{_datadir}/GConf/gsettings/pulseaudio.convert
%{_datadir}/glib-2.0/schemas/org.freedesktop.pulseaudio.gschema.xml

%files libs -f %{name}.lang
%license LICENSE GPL LGPL
%doc README
%dir %{_sysconfdir}/pulse/
%config(noreplace) %{_sysconfdir}/pulse/client.conf
%{_libdir}/libpulse.so.0*
%{_libdir}/libpulse-simple.so.0*
%dir %{_libdir}/pulseaudio/
%{_libdir}/pulseaudio/libpulsecommon-%{version}.so

%files libs-glib2
%{_libdir}/libpulse-mainloop-glib.so.0*

%files libs-devel
%doc %{_vpath_builddir}/doxygen/html
%{_includedir}/pulse/
%{_libdir}/libpulse.so
%{_libdir}/libpulse-mainloop-glib.so
%{_libdir}/libpulse-simple.so
%{_libdir}/pkgconfig/libpulse*.pc
%dir %{_datadir}/vala
%dir %{_datadir}/vala/vapi
%{_datadir}/vala/vapi/libpulse.vapi
%{_datadir}/vala/vapi/libpulse.deps
%{_datadir}/vala/vapi/libpulse-mainloop-glib.vapi
%{_datadir}/vala/vapi/libpulse-mainloop-glib.deps
%{_datadir}/vala/vapi/libpulse-simple.deps
%{_datadir}/vala/vapi/libpulse-simple.vapi
%dir %{_libdir}/cmake
%{_libdir}/cmake/PulseAudio/

%files utils
%{_bindir}/pacat
%{_bindir}/pacmd
%{_bindir}/pactl
%{_bindir}/paplay
%{_bindir}/parec
%{_bindir}/pamon
%{_bindir}/parecord
%{_bindir}/pax11publish
%{_bindir}/pasuspender
%{_mandir}/man1/pacat.1*
%{_mandir}/man1/pacmd.1*
%{_mandir}/man1/pactl.1*
%{_mandir}/man1/pamon.1*
%{_mandir}/man1/paplay.1*
%{_mandir}/man1/parec.1*
%{_mandir}/man1/parecord.1*
%{_mandir}/man1/pasuspender.1*
%{_mandir}/man1/pax11publish.1*
%{bash_completionsdir}/pacat
%{bash_completionsdir}/pacmd
%{bash_completionsdir}/pactl
%{bash_completionsdir}/padsp
%{bash_completionsdir}/paplay
%{bash_completionsdir}/parec
%{bash_completionsdir}/parecord
%{bash_completionsdir}/pasuspender

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 16.1-7
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 16.1-6
- Rebuilt for loongarch release

* Tue Jul 30 2024 rockerzhu <rockerzhu@tencent.com> - 16.1-5
- [Type] bugfix
- [Desc] Fix volume test fails with impossilbe alignments.

* Mon Nov 13 2023 Cunshun Xia <cunshunxia@tencent.com> - 16.1-4
- Rebuilt for libsndfile 1.2.0-4

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 16.1-3
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Aug 18 2023 Wang Guodong <gordonwwang@tencent.com> - 16.1-2
- Rebuilt for orc 0.4.33

* Mon Jul 31 2023 Shuo Wang <abushwang@tencent.com> - 16.1-1
- update to 16.1

* Mon Jul 17 2023 Xiaojie Chen <jackxjchen@tencent.com> - 15.0-4
- Rebuilt for gstreamer1-plugins-base 1.22.4

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 15.0-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 15.0-2
- Rebuilt for OpenCloudOS Stream 23

* Thu Nov 24 2022 cunshunxia <cunshunxia@tencent.com> - 15.0-1
- initial build
